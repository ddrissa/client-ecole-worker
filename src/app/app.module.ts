import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {ServiceWorkerModule} from '@angular/service-worker';
import {AppComponent} from './app.component';

import {environment} from '../environments/environment';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {AccueilComponent} from './shared/accueil/accueil.component';
import {EnseignantModule} from './personne/enseignant/enseignant.module';
import {MatiereModule} from './matiere/matiere.module';
import {DureInterceptor} from './shared/intercepteurs/dure-interceptor';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CachingInterceptor} from './shared/intercepteurs/caching-interceptor';
import {CacheService} from './shared/intercepteurs/cache/cache.service';
import {MessageErreurService} from './shared/service/messageErreur.service';
import {InviteService} from './shared/service/personne/invites/invite.service';
import {InviteComponent} from './personne/invite/invite.component';
import {FormsModule} from '@angular/forms';
import {EmployeModule} from './personne/employe/employe.module';
import {DialogInviteDetailComponent} from './personne/invite/dialog-invite-detail/dialog-invite-detail.component';
import {DialogInviteCrudComponent} from './personne/invite/dialog-invite-crud/dialog-invite-crud.component';
import { DialogInviteSupprimeComponent } from './personne/invite/dialog-invite-supprime/dialog-invite-supprime.component';
import {TextMaskModule} from 'angular2-text-mask';


@NgModule({
  entryComponents: [InviteComponent, DialogInviteDetailComponent, DialogInviteCrudComponent, DialogInviteSupprimeComponent],
  declarations: [
    AppComponent,
    AccueilComponent,
    InviteComponent,
    DialogInviteDetailComponent,
    DialogInviteCrudComponent,
    DialogInviteSupprimeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TextMaskModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
    BrowserAnimationsModule,
    MaterialModule,
    // PrimengModule,
    MatiereModule,
    EnseignantModule,
    EmployeModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: DureInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true},
    InviteService, MessageErreurService, CacheService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
