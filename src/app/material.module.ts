import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  imports: [CommonModule,
    MatButtonModule, MatSidenavModule,
    MatCardModule, MatIconModule, MatToolbarModule,
    MatTooltipModule, MatGridListModule,
    MatProgressBarModule, MatSelectModule,
    MatChipsModule, MatInputModule, MatExpansionModule,
    MatSnackBarModule, MatButtonToggleModule, MatProgressSpinnerModule,
    MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule,
    MatChipsModule, MatDialogModule, MatTabsModule, MatAutocompleteModule
  ],
  exports: [MatButtonModule, MatSidenavModule,
    MatCardModule, MatIconModule, MatToolbarModule,
    MatTooltipModule, MatGridListModule, MatListModule,
    MatProgressBarModule, MatSelectModule,
    MatChipsModule, MatInputModule, MatExpansionModule,
    MatSnackBarModule, MatButtonToggleModule, MatProgressSpinnerModule,
    MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule,
    MatChipsModule, MatDialogModule, MatTabsModule, MatAutocompleteModule],
  declarations: []
})
export class MaterialModule {
}
