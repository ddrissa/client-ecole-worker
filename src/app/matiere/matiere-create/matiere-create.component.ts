import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Matiere} from '../../shared/model/matiere/matiere';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-matiere-create',
  templateUrl: './matiere-create.component.html',
  styleUrls: ['./matiere-create.component.scss']
})
export class MatiereCreateComponent implements OnInit {
  newMatiere: Matiere = new Matiere(null, null, '', '');
  confirm = false;


  constructor(private matiereService: MatiereService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.confirm = true;
    let m: Matiere;
    m = new Matiere();
    this.matiereService.sauver(this.newMatiere).subscribe(res => {
        m = res.body;
        console.log(m.libelle, 'est enregistrer.');
      }
    );
    this.newMatiere = null;
    this.router.navigate(['/matiere/list']);
  }

  annuler() {
    this.router.navigate(['matiere/list']);
  }

  canDeactivate(): boolean {
    console.log('Je navigue ailleur!');
    if (this.confirm === false && this.newMatiere.libelle !== '') {
      // console.log(`L'ajout annuler avec: `, this.newMatiere);
      return window.confirm(`Vous voulez annuler l'ajout? `);
    }
    return true;
  }

}
