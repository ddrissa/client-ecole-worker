import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {Matiere} from '../../shared/model/matiere/matiere';
import {ActivatedRoute, ParamMap, Params, Router} from '@angular/router';
import {  switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-matiere-edit',
  templateUrl: './matiere-edit.component.html',
  styleUrls: ['./matiere-edit.component.scss']
})
export class MatiereEditComponent implements OnInit {
  matiere: Matiere = new Matiere();
  edite: boolean;
  selectedMat: Matiere = new Matiere();


  constructor(private matiereService: MatiereService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    // let id = this.route.snapshot.params['id'];
    this.route.paramMap
      .pipe(switchMap((params: ParamMap) => this.matiereService.getMatiere(+params.get('id'))))
      .subscribe(res => {
        this.matiere = res.body;
        this.edite = false;
      })

    this.matiereService.matiereSelected$
      .subscribe(m => this.selectedMat = m);
  }


  annuler(m: Matiere) {
    this.router.navigate(['matiere/list']);
  }

  onSubmit() {

    this.matiereService.modifierMatiere(this.matiere)
      .subscribe(res => console.log('Matiere modifier', res.body));
    this.router.navigate(['matiere/list']);
    this.selectedMat = this.matiere;
  }

  canDeactivate() {
    console.log('Je navigue ailleur!');
    if (this.selectedMat.libelle !== this.matiere.libelle || this.selectedMat.description !== this.matiere.description) {
      return confirm('Voulez-vous annuler les modifications!');
    }
    return true;
  }
}
