import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatiereNotFindComponent } from './matiere-not-find.component';

describe('MatiereNotFindComponent', () => {
  let component: MatiereNotFindComponent;
  let fixture: ComponentFixture<MatiereNotFindComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatiereNotFindComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatiereNotFindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
