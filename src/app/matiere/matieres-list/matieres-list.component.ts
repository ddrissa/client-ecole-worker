import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {Router} from '@angular/router';
import {Matiere} from '../../shared/model/matiere/matiere';
import {Observable, Subject, BehaviorSubject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {MessageErreurService} from '../../shared/service/messageErreur.service';

@Component({
  selector: 'app-matieres-list',
  templateUrl: './matieres-list.component.html',
  styleUrls: ['./matieres-list.component.scss']
})
export class MatieresListComponent implements OnInit {

  title = 'LES MATIERES.';
  matieres: Matiere[];
  omatieres: Observable<Matiere[]>;
  private searchValeur = new BehaviorSubject<string>(' ');
  selectedMatiere: Matiere;
  errorMessage: string;
  errorMessageStatus: string;
  successMessage = '';
  activeRoute = false;
  mc = '';
  textFitre = '';

  /// todo: Constructeur
  constructor(private  matiererService: MatiereService,
              private router: Router,
              private  msgService: MessageErreurService) {
  }

  ngOnInit() {
    // this.searchValeur.next('');
    this.getAllMatieres();
    // La matiere selectionner.
    this.matiererService.matiereSelected$
      .subscribe(m => {
        this.selectedMatiere = m;
        console.log('Dans list selectedmatiere recuperer ', this.selectedMatiere);
      });

    // La matiere a été creé.
    this.matiererService.matiereCreer$
      .subscribe(res => {
        this.closeMessage();
        this.matieres.push(res.body);
        this.successMessage = res.messages.toString();
        console.log(this.successMessage);
      });
    // Matiere recuperer
    this.matiererService.matiereRecuperer$
      .subscribe(res => {
        this.closeMessage();
        if (res.status !== 0) {
          this.initialiseList();
          this.errorMessageStatus = res.messages.toString();

        }
      });
    // La matiere a été modifier.
    this.matiererService.matiereModifier$
      .subscribe(res => {
        this.closeMessage();
        let mats: Matiere[];
        mats = [...this.matieres];
        console.log('index', this.findSelectedMatiereIndex());
        mats[this.findSelectedMatiereIndex()] = res.body;
        // console.log('index', this.findSelectedMatiereIndex());
        this.successMessage = res.messages.toString();
        console.log(this.successMessage);
        console.log('Composant', mats);
        console.log('Composant 2', res.body);
        this.matieres = mats;

      });
    // La matiere a été supprimer.
    this.matiererService.matiereSupprimer$
      .subscribe(mat => {
        this.closeMessage();
        let index: number;
        index = this.findSelectedMatiereIndex();
        this.matieres = this.matieres.filter((val, i) => i !== index);
        this.successMessage = `La matière ${mat.id} ${mat.libelle} a été supprimer.`;
        this.selectedMatiere = mat;
        console.log(this.successMessage);
        return this.matieres;
      });

    this.matiererService.matiereFiltre$
      .subscribe(t => this.search(t));

    // Rechercher
    this.omatieres = this.searchValeur
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(v => v ? this.matiererService.seachMatiere(v) : this.matiererService.seachMatiere(' '))
      );


    /// A la fin
    this.log();
    if (this.errorMessage) {
      console.log(this.errorMessage);
    }

  }

  getAllMatieres() {
    this.matiererService.getAllMatieres()
      .subscribe(data => {
          this.matieres = data.body;
          console.log(data);
        },
        error => {
          if (error.status = 0) {
            this.errorMessageStatus = 'Problème de connexion.';
          } else {
            this.errorMessage = error;
          }
          ;
        }
      );
  }


  // Rechercher une matiere
  search(v: string) {
    this.searchValeur.next(v);
  }

  // isSelected(matiere: Imatiere) {
  //   return matiere.id === this.selectedMatiere.id;
  // }

  onSelect(matiere: Matiere) {
    this.selectedMatiere = matiere;
    // this.matiererService.matiereSelectStream(this.selectedMatiere);
    this.router.navigate(['matiere/list/detail', matiere.id]);

  }

  findSelectedMatiereIndex(): number {
    return this.matieres.indexOf(this.selectedMatiere);
  }

  private closeMessage() {
    setTimeout(() => {
      this.successMessage = '';
      this.errorMessageStatus = '';
      this.errorMessage = ''
    }, 5000);
  }

  /// todo : recuperer les messages d'erreur
  private log() {
    this.msgService.message$.subscribe(ms => {
      this.errorMessage = ms;
      console.log('Message erreur dans service; ', ms);
    });
  }

/// todo: initialiser
  private initialiseList() {
    setTimeout(() => {

      this.search(' ');
    }, 5000);
  };
}
