import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MatiereManageComponent} from './matiere-manage/matiere-manage.component';
import {AuthMatiereGuard} from '../shared/guard/matiere/auth-matiere.guard';
import {MatieresListComponent} from './matieres-list/matieres-list.component';
import {MatiereInviteComponent} from './matiere-invite/matiere-invite.component';
import {MatiereDetailComponent} from './matiere-detail/matiere-detail.component';
import {MatiereDetailResolveService} from '../shared/service/matiere/resolve/matiere-detail-resolve.service';
import {MatiereEditComponent} from './matiere-edit/matiere-edit.component';
import {CanDesactiveMatiereGuard} from '../shared/guard/matiere/can-desactive-matiere.guard';
import {MatiereCreateComponent} from './matiere-create/matiere-create.component';

const routes: Routes = [
  {
    path: 'matiere',
    children: [
      {
        path: '', component: MatiereManageComponent
      },

      {
        path: 'list',
        canActivate: [AuthMatiereGuard],
        component: MatieresListComponent,
        canActivateChild: [AuthMatiereGuard],
        children: [
          {path: '', component: MatiereInviteComponent},
          {
            path: 'detail/:id', component: MatiereDetailComponent,
            resolve: {resultat: MatiereDetailResolveService}
          },
          {
            path: 'edit/:id', component: MatiereEditComponent,
            canDeactivate: [CanDesactiveMatiereGuard]
          },
          {
            path: 'create', component: MatiereCreateComponent,
            canDeactivate: [CanDesactiveMatiereGuard]
          }
        ]
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatiereRoutingModule { }
