import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatiereInviteComponent } from './matiere-invite.component';

describe('MatiereInviteComponent', () => {
  let component: MatiereInviteComponent;
  let fixture: ComponentFixture<MatiereInviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatiereInviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatiereInviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
