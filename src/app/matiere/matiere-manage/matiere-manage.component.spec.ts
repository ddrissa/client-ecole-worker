import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatiereManageComponent } from './matiere-manage.component';

describe('MatiereManageComponent', () => {
  let component: MatiereManageComponent;
  let fixture: ComponentFixture<MatiereManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatiereManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatiereManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
