import { TestBed, async, inject } from '@angular/core/testing';

import { CanDesactiveMatiereGuard } from './can-desactive-matiere.guard';

describe('CanDesactiveMatiereGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanDesactiveMatiereGuard]
    });
  });

  it('should ...', inject([CanDesactiveMatiereGuard], (guard: CanDesactiveMatiereGuard) => {
    expect(guard).toBeTruthy();
  }));
});
