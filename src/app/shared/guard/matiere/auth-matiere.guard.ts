import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable()
export class AuthMatiereGuard implements CanActivate, CanActivateChild {
  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log('Vous être autoriser aller voir les matieres!');
    return true;
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log(`Un enfant est entrain d'être activé!`);
    return  window.confirm('Avez le droit de modidier ou ajouter une matiere?');
  }
}
