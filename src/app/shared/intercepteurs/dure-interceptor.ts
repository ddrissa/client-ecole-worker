import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {InviteService} from '../service/personne/invites/invite.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {MatiereService} from '../service/matiere/matiere.service';

@Injectable()
export class DureInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const debut = Date.now();
    return next.handle(req)
      .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            const duree = Date.now() - debut;
            console.log(`La requete de ${req.urlWithParams} a duré ${duree} ms `);
          }
        })
      );

  }
}
