import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

import {CacheService} from './cache/cache.service';
import {concat} from 'rxjs/internal/observable/concat';
import {of} from 'rxjs/internal/observable/of';


@Injectable()
export class CachingInterceptor implements HttpInterceptor {


  constructor(private cache: CacheService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Ignore toujours les requêtes non-GET
    if (req.method !== 'GET') {
      return next.handle(req);
    }

    // Ce sera un observable de la valeur mise en cache s'il y en a une,
    // ou autrement un observable vide . Ça commence vide.

    let maybeCachedResponse: Observable<HttpEvent<any>> = of();
    // Vérifie le cache.
    const cachedResponse = this.cache.get(req);
    if (cachedResponse) {
      maybeCachedResponse = of(cachedResponse);
      console.log('RRPONSE DU CACHE', maybeCachedResponse);

    }
    // Crée un observable (mais ne t'abonne pas) qui représente
    // la requête réseau et la mise en cache de la valeur.
    const networkResponse = next.handle(req).pipe(
      tap(event => {
        // Comme auparavant, vérifiez l'événement HttpResponse et mettez-le en cache.
        if (event instanceof HttpResponse) {
          this.cache.put(req, event);
          console.log('Mise a jour cache reseau', event.body);
        }
      })
    );

    // Maintenant, combinez les deux et envoyez d'abord la réponse mise en cache (s'il y aun),
    // et la réponse du réseau en second lieu.

    return concat(maybeCachedResponse, networkResponse);
  }
}
