import {Injectable} from '@angular/core';
import {HttpCache} from './http-cache';
import {HttpRequest, HttpResponse} from '@angular/common/http';

@Injectable()
export class CacheService extends HttpCache {
  private cache = new Map<string, HttpResponse<any>>();

  constructor() {
    super();
  }


  get(req: HttpRequest<any>): HttpResponse<any> | null {

    return this.cache.get(req.urlWithParams);
  }

  put(req: HttpRequest<any>, rep: HttpResponse<any>): void {
    this.cache.set(req.urlWithParams, rep.clone());
  }
}
