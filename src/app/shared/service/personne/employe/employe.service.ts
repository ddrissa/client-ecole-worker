import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MessageErreurService} from '../../messageErreur.service';
import {Administrateur} from '../../../model/personne/administrateur';
import {Resultat} from '../../../model/resultat';
import {Observable, of, Subject} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class EmployeService {

  private _urlEmpl = 'http://localhost:8080/typepersonnes/EN';

  private _url = 'http://localhost:8080/personnes';

  private _url_rech = 'http://localhost:8080/persrech/EN?mc=';
  // photos url pathphoto 1
  private _url_photos = 'http://localhost:8080/photos/';


  // communication sources entre composant
  private employeSourceCreer = new Subject<Resultat<Administrateur>>();
  private employeSourceModifier = new Subject<Resultat<Administrateur>>();
  private employeSourceSupprime = new Subject<Resultat<boolean>>();
  // flux de communication
  ceerEmployer$ = this.employeSourceCreer.asObservable();
  modifieEmployer$ = this.employeSourceModifier.asObservable();
  supprmrEmployer$ = this.employeSourceSupprime.asObservable();

  constructor(private _http: HttpClient,
              private  messageService: MessageErreurService) {
  }

  getAllEmployes(): Observable<Resultat<Array<Administrateur>>> {
    return this._http.get<Resultat<Array<Administrateur>>>(this._urlEmpl)
      .pipe(
        tap((res: Resultat<Array<Administrateur>>) => console.log('Ad')),
        catchError(this.handleError<Resultat<Array<Administrateur>>>('getAllEmployes',
          new Resultat<Array<Administrateur>>(null, [], [])))
      );
  }


  creerEmployer(emp: Administrateur): Observable<Resultat<Administrateur>> {
    emp.type = 'AD';
    return this._http.post<Resultat<Administrateur>>(this._url, emp, httpOptions)
      .pipe(
        tap(res => {
          this.employeCreer(res);
          console.log('Dans le Service employe creer :', res);
        }),
        catchError(this.handleError<Resultat<Administrateur>>('creerEployer',
          new Resultat<Administrateur>(null, [], null)))
      );
  }

  employeById(id: number): Observable<Resultat<Administrateur>> {
    return this._http.get<Resultat<Administrateur>>(`${this._url}/${id}`)
      .pipe(
        tap(res => console.log('Service Employe trouvee: ', res))
      );
  }


  modifierEmploye(emp: Administrateur): Observable<Resultat<Administrateur>> {
    return this._http.put<Resultat<Administrateur>>(this._url, emp, httpOptions).pipe(
      tap(res => {
        this.employeModifie(res);
        console.log(`Dans l'employe ${res.body} a ete modifier.`);
      }),
      catchError(this.handleError<Resultat<Administrateur>>('modifierEmploye',
        new Resultat<Administrateur>(null, [], null)))
    );
  }

  supprumerEmploye(id: number): Observable<Resultat<boolean>> {
    return this._http.delete<Resultat<boolean>>(`${this._url}/${id}`)
      .pipe(
        tap(res => {
          this.employeSupprime(res);
          console.log(`L'employe d'identifiant ${id} a ete supprimer avec succes.`)
        }),
        catchError(this.handleError<Resultat<boolean>>('supprimeEmploye'))
      );
  }


  /////////////////////////////////////////////////
  // Communication
  employeCreer(r: Resultat<Administrateur>) {
    this.employeSourceCreer.next(r);
  }

  employeModifie(r: Resultat<Administrateur>) {
    this.employeSourceModifier.next(r);
  }
  employeSupprime(r: Resultat<boolean>) {
    this.employeSourceSupprime.next(r);
  }


  // Gestion des erreurs

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: envoie l'erreur à l'infrastructure de journalisation à distance
      console.error(error);
      // TODO: La bonne mamière de transformer le message d'erreur pour etre consommé
      this.log(`${operation} à échoué: ${error.message}`);

      // retourne un resultat vide pour aue l'app puisse continuer de fonctionner.
      return of(result as T);
    };


  }

  //////////////
  private log(message: string) {
    this.messageService.ajoutMessage(message);
  }
}
