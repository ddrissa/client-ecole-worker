import {Injectable} from '@angular/core';
import {Observable, Subject, of} from 'rxjs';
import {Resultat} from '../../../model/resultat';
import {Enseignant} from '../../../model/personne/enseignant';
import {Adresse} from '../../../model/personne/adresse';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {MessageErreurService} from '../../messageErreur.service';
import {catchError, tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class EnseignantService {
  private urlEnseigns = 'http://localhost:8080/typepersonnes/EN';

  private _url = 'http://localhost:8080/personnes';

  private _url_rech = 'http://localhost:8080/persrech/EN?mc=';
  // photos url pathphoto 1
  private _url_photos = 'http://localhost:8080/photos/';

/*
  // photos url photo 2
  private _url_photos2 = 'http://localhost:8080/photo/';
  // photos url photo recuperer maniere 2
  private _url_photo = 'http://localhost:8080/photo2/';
*/

  // Enseignant source
  private enseignantCreerSource = new Subject<Resultat<Enseignant>>();
  private enseignantModifSource = new Subject<Resultat<Enseignant>>();
  private enseignantSupprimeSource = new Subject<Resultat<boolean>>();
  private enseignantFilterSource = new Subject<string>();
  private reponseStatutSource = new Subject<number>();

  // Stream
  enseignantCreer$ = this.enseignantCreerSource.asObservable();
  enseignantModifier$ = this.enseignantModifSource.asObservable();
  enseignantSupprime$ = this.enseignantSupprimeSource.asObservable();
  enseignantFilter$ = this.enseignantFilterSource.asObservable();
  reponseStatut$ = this.reponseStatutSource.asObservable();

  constructor(private _http: HttpClient, private messageService: MessageErreurService) {
  }

// La liste de tous les enseignant
  getAllEnseignant(): Observable<Resultat<Array<Enseignant>>> {
    return this._http.get<Resultat<Array<Enseignant>>>(this.urlEnseigns);
  }


  // Ajouter un enseignant
  ajoutEnseignant(ens: Enseignant): Observable<Resultat<Enseignant>> {
    // chemin photo
    // const pathph = `${this._url_photos}${ens.numCni}`;
    // L'adresse de l'enseignant
    const adr = new Adresse(ens.adresse.quartier, ens.adresse.codePostal,
      ens.adresse.email, ens.adresse.mobile, ens.adresse.bureau, ens.adresse.tel);
    // L'enseignant cree avec l'adresse;
    const newEnseig = new Enseignant(null, 0, ens.titre, adr, ens.nom, ens.prenom,
      ens.numCni, ens.login, ens.password, ens.actived, 'EN', ens.pathphoto, ens.telephones, ens.nomComplet, ens.status,
      ens.specialite);


    return this._http.post<Resultat<Enseignant>>(this._url, newEnseig, httpOptions)
      .pipe(
        tap(rep => {
          this.filtrerText(rep.body.nomComplet);
          this.statutReponse(rep.status);
          this.enseignantCreer(rep);
        }),
      );
  }


  // Modifier un enseignant
  modifierEnseignant(ens: Enseignant): Observable<Resultat<Enseignant>> {
    // chemin photo
    //  const pathph = `${this._url_photos}${ens.numCni}`;
    // L'adresse de l'enseignant
    const adr = new Adresse(ens.adresse.quartier, ens.adresse.codePostal,
      ens.adresse.email, ens.adresse.mobile, ens.adresse.bureau, ens.adresse.tel);
    // L'enseignant cree avec l'adresse;
    const modEns = new Enseignant(ens.id, ens.version, ens.titre, adr, ens.nom, ens.prenom,
      ens.numCni, ens.login, ens.password, ens.actived, 'EN', ens.pathphoto, ens.telephones, ens.nomComplet, ens.status,
      ens.specialite);

    return this._http.put<Resultat<Enseignant>>(this._url, modEns, httpOptions)
      .pipe(
        tap(rep => {
          this.filtrerText(rep.body.nomComplet);
          this.enseignantModif(rep);
        }),
      );

  }

  // Trouver un enseignant par id
  getEnseignatById(id: number): Observable<Resultat<Enseignant>> {
    return this._http.get<Resultat<Enseignant>>(`${this._url}/${id}`);
  }

  // Trouver un ensegnant par motcle
  searchEnsMotcle(mc: string): Observable<Resultat<Array<Enseignant>>> {
    return this._http.get<Resultat<Array<Enseignant>>>(`${this._url_rech}${mc}`)
      .pipe(
        tap(rep => console.log(`Enseignants trouver avec ${mc} sont :`, rep.body)),
        catchError(
          this.handleError<Resultat<Array<Enseignant>>>(
            'searchMcle', new Resultat<Array<Enseignant>>(0, [], []))),
      );
  }

  // Supprimer un enseignant
  supprimerEnseignat(id: number): Observable<Resultat<boolean>> {
    return this._http.delete<Resultat<boolean>>(`${this._url}/${id}`)
      .pipe(
        tap(rep => {
          this.filtrerText('');
          this.enseignantSuprime(rep);
        }),
      );
  }

// Recuperer le lien d'image pour <img src=getPhoto(id)
  getPhotos(cni: string) {
    return `${this._url_photos}${cni}`;

  }

  // recuperer une pathphoto par reponse
/*  getPhoto(cni: string): Observable<Resultat<File>> {
    return this._http.get<Resultat<File>>(`${this._url_photo}${cni}`);

  }*/

  // Ajouter une pathphoto a l'enseignnt
  ajoutPhoto(file: File, cni: string): Observable<any> {
    // Forme de donnee du formulaire
    const data = new FormData();

    data.append('imageFile', file, cni);
    const req = new HttpRequest('POST', this._url_photos, data, {reportProgress: true});
    return this._http.request(req)
      .pipe(
        tap(event => {
         if (event instanceof HttpResponse){
           this.filtrerText(event.body.body.nomComplet);
            this.enseignantModif(event.body);
           console.log('Service photo :', event.body);
         }
        }),
      );
  }

  // Creer un enseignant avec la pathphoto
  /* creerEnseignPhoto(file: File, ens: Enseignant): Observable<Resultat<Enseignant>> {
     const fdata = new FormData();
     fdata.append('enseignant', ens);
     fdata.append('image', file, ens.numCni);
     return this._http.post<Resultat<Enseignant>>(this._url_photos2, fdata));

   }*/

///////////////////////// Sources///////////////////////////
  private enseignantCreer(r: Resultat<Enseignant>) {
    this.enseignantCreerSource.next(r);
  }

  private enseignantModif(r: Resultat<Enseignant>) {
    this.enseignantModifSource.next(r);
  }

  private enseignantSuprime(r: Resultat<boolean>) {
    this.enseignantSupprimeSource.next(r);
  }

  private filtrerText(t: string) {
    this.enseignantFilterSource.next(t);
  }

  private statutReponse(num: number) {
    this.reponseStatutSource.next(num);
  }

  /////////////////////////////////////////////////////////////
  // manipuler les erreurs

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: envoye l'erreur a la console
      console.error(error); // log to console instead

      // TODO: La bonne mamière de transformer le message d'erreur pour etre consommé
      this.log(`${operation} a échoué: ${error.message}`);

      // retourne un resultat vide pour aue l'app puisse continuer de fonctionner.
      return of(result as T);
    };
  }

  /** enregistrer un message avec MessageErreurService */
  private log(message: string) {
    this.messageService.ajoutMessage('EnseignantService: ' + message);
  }


}
