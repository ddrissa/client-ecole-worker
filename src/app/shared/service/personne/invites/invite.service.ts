import {Injectable} from '@angular/core';

import {Resultat} from '../../../model/resultat';
import {Invite} from '../../../model/personne/invite';
import {Observable, of} from 'rxjs';
import {Adresse} from '../../../model/personne/adresse';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {MessageErreurService} from '../../messageErreur.service';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class InviteService {
// TODO: url tous les titre IN
  private urlInvites = 'http://localhost:8080/typepersonnes/IN';
  // todo: url invites
  private _url = 'http://localhost:8080/personnes';

  constructor(private  http: HttpClient, private messageService: MessageErreurService) {
  }

  // todo: les invités
  getAllsInvites(): Observable<Resultat<Array<Invite>>> {
    return this.http.get<Resultat<Array<Invite>>>(this.urlInvites).pipe(
      tap((res: Resultat<Array<Invite>>) => console.log('liste rcupere:', res)),
      catchError(this.handleError<Resultat<Array<Invite>>>('getAllsInvites', new Resultat<Array<Invite>>(null, [], [])))
    );

  }


  // todo :ajouter un invité

  ajoutInvite(invite: Invite): Observable<Resultat<Invite>> {
    const ad1 = new Adresse(
      invite.adresse.quartier,
      invite.adresse.codePostal,
      invite.adresse.email,
      invite.adresse.mobile,
      invite.adresse.bureau,
      invite.adresse.tel);

    const newInvite = new Invite(null, null, invite.titre,
      ad1, invite.nom, invite.prenom, invite.numCni, invite.login, invite.password,
      invite.actived, 'IN', null, invite.raison, invite.profil, invite.societe, invite.status);
    return this.http.post<Resultat<Invite>>(this._url, newInvite, httpOptions)
      .pipe(
        tap((res: Resultat<Invite>) => console.log('Ajout de :', res)),
        catchError(this.handleError<Resultat<Invite>>('ajouterInvite'))
      );

  }


  // todo: modifier un inviter

  editerInvitet(invite: Invite): Observable<Resultat<Invite>> {

    const ad = new Adresse(invite.adresse.quartier, invite.adresse.codePostal,
      invite.adresse.email, invite.adresse.mobile, invite.adresse.bureau, invite.adresse.tel);


    const editInvite = new Invite(invite.id, invite.version, invite.titre, ad,
      invite.nom, invite.prenom, invite.numCni, invite.login, invite.password, invite.actived,
      'IN', invite.nomComplet, invite.raison, invite.profil, invite.societe, invite.status);
    return this.http.put<Resultat<Invite>>(this._url, editInvite, httpOptions)
      .pipe(
        tap((res: Resultat<Invite>) => console.log('Modification de :', res)),
        catchError(this.handleError<Resultat<Invite>>('ajouterInvite'))
      );
  }

  /*todo: supprimer d'invite*/
  suppimeInvite(id: number): Observable<Resultat<boolean>> {
    return this.http.delete<Resultat<boolean>>(`${this._url}/${id}`, httpOptions)
      .pipe(
        tap((res: Resultat<boolean>) => console.log('Invite supprime :', res)),
        catchError(this.handleError<Resultat<boolean>>('ajouterInvite'))
      );


  }


  /////////////////////////////////////////////////////////////
  // manipuler les erreurs

  /**
   * Manipuler l'operation Http  qui echoué.
   * Laisser  l'app continuer.
   * @param operation - nom de l'operation qui a echoué
   * @param result - optional valeur retourner comme resultat observable
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: envoye l'erreur a la console
      console.error(error); // log to console instead

      // TODO: La bonne mamiere de transformer le message d'erreur pour etre consommé
      this.log(`${operation} echoué: ${error.message}`);

      // retourne un resultat vide pour aue l'app puisse continuer de fonctionner.
      return of(result as T);
    };
  }

  /** enregistrer un message avec MessageErreurService */
  private log(message: string) {
    this.messageService.ajoutMessage('InviteService: ' + message);
  }

}
