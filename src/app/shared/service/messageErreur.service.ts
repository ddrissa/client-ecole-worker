import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class MessageErreurService {
  private messageSource = new Subject<string>();
  message$ = this.messageSource.asObservable();

  constructor() {
  }


  ajoutMessage(msg: string) {
    this.messageSource.next(msg);
  };
}
