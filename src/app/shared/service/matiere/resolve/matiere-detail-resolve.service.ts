import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Matiere} from '../../../model/matiere/matiere';
import {Observable} from 'rxjs';
import {MatiereService} from '../matiere.service';
import {Resultat} from '../../../model/resultat';

@Injectable()
export class MatiereDetailResolveService implements Resolve<Resultat<Matiere>> {

  constructor(private matiereService: MatiereService, private router: Router) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Resultat<Matiere>>  {
    const id = +route.paramMap.get('id');
    return this.matiereService.getMatiere(id).pipe(resultat => {
      if (resultat) {
       /* console.log('Resolve: ', resultat);*/
        return resultat;
      } else {
        // this.matiereService.notfind(resultat.messages);
        this.router.navigate(['matiere/list']);
        return null;
      }
    });
  }
}
