import {Injectable} from '@angular/core';
import {Observable, Subject, of} from 'rxjs';
import {Resultat} from '../../model/resultat';
import {Matiere} from '../../model/matiere/matiere';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {MessageErreurService} from '../messageErreur.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class MatiereService {

  private _url = 'http://localhost:8080/matieres';
  private _url_search = 'http://localhost:8080/matrech?mc=';
  // observable source.
  private matiereCreerSource = new Subject<Resultat<Matiere>>();
  private matiereModifierSource = new Subject<Resultat<Matiere>>();
  private matiereRecupererSource = new Subject<Resultat<Matiere>>();
  private matiereSupprimerSource = new Subject<Matiere>();
  private matiereSelectedSource = new Subject<Matiere>();
  private matiereFiltreSource = new Subject<string>();
  private matiereNotFindSource = new Subject<string[]>();

  // observable stream.
  matiereCreer$ = this.matiereCreerSource.asObservable();
  matiereModifier$ = this.matiereModifierSource.asObservable();
  matiereRecuperer$ = this.matiereRecupererSource.asObservable();
  matiereSupprimer$ = this.matiereSupprimerSource.asObservable();
  matiereSelected$ = this.matiereSelectedSource.asObservable();
  matiereFiltre$ = this.matiereFiltreSource.asObservable();
  matiereNotFind$ = this.matiereNotFindSource.asObservable();

  constructor(private _http: HttpClient, private messageService: MessageErreurService) {
  }

///////////////////////////////////////
  /*
   getAlllMatiere
   * */
  getAllMatieres(): Observable<Resultat<Array<Matiere>>> {
    return this._http.get<Resultat<Array<Matiere>>>(this._url)
      .pipe(
        tap((res: Resultat<Array<Matiere>>) => console.log('liste recupére:', res)),
        catchError(this.handleError<Resultat<Array<Matiere>>>('getAllsInvites',
          new Resultat<Array<Matiere>>(null, [], [])))
      );


  }

  getMatiere(id: number): Observable<Resultat<Matiere>> {
    return this._http.get<Resultat<Matiere>>(`${this._url}/${id}`, httpOptions)
      .pipe(
        tap((res: Resultat<Matiere>) => {
          this.matiereRecuperer(res);
          console.log(`Matiere recuperer de ${id}:`, res);
        }),
        catchError(this.handleError<Resultat<Matiere>>(`getMatiere id=${id}`))
      );
  }

// Rechercher par mot clé
  seachMatiere(mc: string): Observable<Array<Matiere>> {
    return this._http.get<Resultat<Array<Matiere>>>(`${this._url_search}${mc}`, httpOptions)
      .pipe(map(res => res.body,
        tap(mats => console.log(`Trouver avec  ${mc} sont:`, mats))),
        catchError(this.handleError<Array<Matiere>>(`searchMatiere avec ${mc}`, []))
      );
  }


  sauver(mat: Matiere): Observable<Resultat<Matiere>> {
    return this._http.post<Resultat<Matiere>>(this._url, mat, httpOptions)
      .pipe(
        tap((res: Resultat<Matiere>) => {
          this.filtreText(res.body.libelle);
          this.matiereCreer(res);
          console.log('Service ajout de :', res);
        }),
        catchError(this.handleError<Resultat<Matiere>>('sauver'))
      );
  }

  modifierMatiere(mat: Matiere): Observable<Resultat<Matiere>> {
    return this._http.put<Resultat<Matiere>>(this._url, mat, httpOptions)
      .pipe(
        tap((res: Resultat<Matiere>) => {
          this.filtreText(res.body.libelle);
          this.matiereModifier(res);
          console.log('Service modification de :', res);
        }),
        catchError(this.handleError<Resultat<Matiere>>('modifierMatiere'))
      );
  }

  supprimeerMatiere(id: number): Observable<Resultat<boolean>> {
    return this._http.delete<Resultat<boolean>>(`${this._url}/${id}`, httpOptions)
      .pipe(
        tap((res: Resultat<boolean>) => {
          this.filtreText(' ');
          console.log('Service ajout de :', res);
        }),
        catchError(this.handleError<Resultat<boolean>>('ajouterInvite'))
      );
  }

  /// TODO: COMMUNICATION
// Une matiere a été creé on ajoute l'indfo à notre stream
  matiereCreer(res: Resultat<Matiere>) {
    console.log('matiere a été creé');
    this.matiereCreerSource.next(res);
  }

// Une matiere a été modifier, on ajoute l'indfo notre stream
  matiereModifier(res: Resultat<Matiere>) {
    console.log('matiere a été modifier');
    this.matiereModifierSource.next(res);
  }
  // Une matiere a été rcuperer, on ajoute l'indfo notre stream
  matiereRecuperer(res: Resultat<Matiere>) {
    console.log('matiere a été modifier');
    this.matiereRecupererSource.next(res);
  }

// Une matiere a été supprimer, on ajoute l'indfo notre stream
  matiereSupprimer(mat: Matiere) {
    this.matiereSupprimerSource.next(mat);
  }

  matiereSelectStream(m: Matiere) {
    this.matiereSelectedSource.next(m);
  }

  filtreText(t: string) {
    this.matiereFiltreSource.next(t);
  }

  notfind(msg: string[]) {
    this.matiereNotFindSource.next(msg);
  }

/////////////////////////////////////////////////////////////
// manipuler les erreurs

  // manipuler les erreurs

  /**
   * Manipuler l'operation Http  qui echoué.
   * Laisser  l'app continuer.
   * @param operation - nom de l'operation qui a echoué
   * @param result - optional valeur retourner comme resultat observable
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: envoye l'erreur a la console
      console.error(error); // log to console instead

      // TODO: La bonne mamiere de transformer le message d'erreur pour etre consommé
      this.log(`${operation} a échoué: ${error.message}`);

      // retourne un resultat vide pour que l'app puisse continuer de fonctionner.
      return of(result as T);
    };
  }

  /** enregistrer un message avec MessageErreurService */
  private log(message: string) {
    this.messageService.ajoutMessage('InviteService: ' + message);
  }


}
