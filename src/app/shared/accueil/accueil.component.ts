
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  title = `GESTION D'ECOLE POUR DONNER UNE FORMATIONS DE QUALITE ET D'EXCELLENCES! `;

 /* options: any;

  overlays: any[];
  dialogVisible: boolean;

  markerTitle: string;

  selectedPosition: any;

  infoWindow: any;

  draggable: boolean;

  msgs: Message[] = [];*/

  constructor(private router: Router) {
  }

  onInvite() {
    this.router.navigate(['/invite']);
  }
  onMatiere() {
    this.router.navigate(['/matiere']);
  }

  onEtudiant() {
    this.router.navigate(['/etudiant']);
  }

  onEnsignant() {
    this.router.navigate(['/enseignant']);
  }

  onEmploye() {
    this.router.navigate(['/employe']);
  }

  ngOnInit() {

  /*  this.options = {
      center: {lat: 5.35417509559776, lng: -4.065536119060198},
      zoom: 13
    };
    this.initOverlays();
    this.infoWindow = new google.maps.InfoWindow();
  }


  // Gmap

  handleMapClick(event) {
    this.dialogVisible = true;
    this.selectedPosition = event.latLng;
  }

  handleOverlayClick(event) {
    this.msgs = [];
    let isMarker = event.overlay.getTitle !== undefined;

    if (isMarker) {
      let title = event.overlay.getTitle();
      this.infoWindow.setContent('' + title + '');
      this.infoWindow.open(event.map, event.overlay);
      event.map.setCenter(event.overlay.getPosition());

      this.msgs.push({severity: 'info', summary: 'Marker Selected', detail: title});
    }
    else {
      this.msgs.push({severity: 'info', summary: 'Shape Selected', detail: ''});
    }
  }

  addMarker() {
    this.overlays.push(new google.maps.Marker({
      position: {
        lat: this.selectedPosition.lat(),
        lng: this.selectedPosition.lng()
      }, title: this.markerTitle, draggable: this.draggable
    }));
    this.markerTitle = null;
    this.dialogVisible = false;
  }

  handleDragEnd(event) {
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'Marker Dragged', detail: event.overlay.getTitle()});
  }

  zoomIn(map) {
    map.setZoom(map.getZoom() + 1);
  }

  zoomOut(map) {
    map.setZoom(map.getZoom() - 1);
  }

  clear() {
    this.overlays = [];
  }


  initOverlays() {
    if (!this.overlays || !this.overlays.length) {
      this.overlays = [
        new google.maps.Marker({position: {lat: 5.35417509559776, lng: -4.065536119060198}, title: 'Kossovo'}),
      ];
    }
  }*/

}
}
