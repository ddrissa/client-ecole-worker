import {Personne} from './personne';
import {Adresse} from './adresse';
import {Iinvite} from './interface/personne/iinvite';
import {Iadresse} from './interface/personne/iadresse';

export class Invite implements Iinvite {


  constructor(public id?: number,
              public version?: number,
              public titre?: string,
              public adresse?: Iadresse,
              public nom?: string,
              public prenom?: string,
              public numCni?: string,
              public login?: string,
              public password?: string,
              public actived?: boolean,
              public type?: string,
              public nomComplet?: string,
              public raison?: string,
              public profil?: string,
              public societe?: string,
              public status?: string) {

  }


}
