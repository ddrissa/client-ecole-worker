import {Personne} from './personne';
import {Adresse} from './adresse';
import {Telephone} from './telephone';

export class Enseignant extends Personne {

  constructor(public id?: number,
              public version?: number,
              public titre?: string,
              public adresse?: Adresse,
              public nom?: string,
              public prenom?: string,
              public numCni?: string,
              public login?: string,
              public password?: string,
              public actived?: boolean,
              public type?: string,
              public pathphoto?: string,
              public telephones?: Telephone[],
              public nomComplet?: string,
              public status?: string,
              public specialite?: string) {
    super(id, version, titre, adresse, nom, prenom, numCni, login,
      password, actived, type, pathphoto, telephones, nomComplet);
  }
}
