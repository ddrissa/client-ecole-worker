import {Iadresse} from './iadresse';
export interface Ipersonne {
  id?: number;
  version?: number;
  titre?: string;
  adresse?: Iadresse;
  nom?: string;
  prenom?: string;
  numCni?: string;
  login?: string;
  password?: string;
  actived?: boolean;
  type?: string;
  nomComplet?: string;
}
