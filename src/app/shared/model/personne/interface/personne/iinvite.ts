import {Ipersonne} from './ipersonne';
export interface Iinvite extends Ipersonne{
  raison?: string;
  profil?: string;
  societe?: string;
  status?: string;

}
