export class Resultat<T> {

  constructor(private _status: number, private _messages: string[], private  _body: T) {
  }


  get status(): number {
    return this._status;
  }

  set status(value: number) {
    this._status = value;
  }

  get messages(): string[] {
    return this._messages;
  }

  set messages(value: string[]) {
    this._messages = value;
  }

  get body(): T {
    return this._body;
  }

  set body(value: T) {
    this._body = value;
  }
}
