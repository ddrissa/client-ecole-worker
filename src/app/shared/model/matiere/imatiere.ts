export interface Imatiere {
  id?: number;
  version?: number;
  libelle?: string;
  description?: string;
}
