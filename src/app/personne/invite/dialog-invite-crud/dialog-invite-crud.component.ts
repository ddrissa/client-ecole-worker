import {Component, Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Invite} from '../../../shared/model/personne/invite';
import emailMask from 'text-mask-addons/dist/emailMask';

@Component({
  selector: 'app-dialog-invite-crud',
  templateUrl: './dialog-invite-crud.component.html',
  styleUrls: ['./dialog-invite-crud.component.scss']
})
export class DialogInviteCrudComponent {
  maskPhone = ['(', '+', '2', '2', '5', ')', ' ', /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  /*cni Variable*/
  /*maskCni = [/[A-Z]/i, /[A-Z]/i, /[A-Z]/i, '-', /\d/, /\d/, /\d/, /\d/];*/
  maskCni = ['C', 'N', 'I', '-', /\d/, /\d/, /\d/];
  emailMask = emailMask;

  titres = ['Mme', 'Mlle', 'Mr', 'Me', 'Dr'];
  profils = ['Client', 'Partenaire', 'Medecin', 'Autre'];
  statuts = ['Permanent', 'Temporaire', 'Occasionnel', 'Autre'];


  constructor(public dialogRef: MatDialogRef<DialogInviteCrudComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Invite) {
  }


  onNoClick() {
    this.dialogRef.close();
  }


}
