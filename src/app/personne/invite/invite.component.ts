import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {InviteService} from '../../shared/service/personne/invites/invite.service';
import {Invite} from '../../shared/model/personne/invite';
import {Iinvite} from '../../shared/model/personne/interface/personne/iinvite';
import {Adresse} from '../../shared/model/personne/adresse';

// import {ConfirmationService, MenuItem, Message, SelectItem} from 'primeng/primeng';
import {MessageErreurService} from '../../shared/service/messageErreur.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {DialogInviteDetailComponent} from './dialog-invite-detail/dialog-invite-detail.component';
import {DialogInviteSupprimeComponent} from './dialog-invite-supprime/dialog-invite-supprime.component';
import {DialogInviteCrudComponent} from './dialog-invite-crud/dialog-invite-crud.component';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit, AfterViewInit {

  /*MATERIAL DEFINITIONS*/
  inviteDataSource = new MatTableDataSource<Invite>();
  displayedColumns = ['id', 'titre', 'numCni', 'nom', 'prenom', 'profil', 'raison', 'action'];
  indexSupp: number;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ///// fin material ////

  invites: Invite[] = [];
  statusSucces: number;
  invite: Invite;
  selectedInvite: Invite;
  displayDialog: boolean;
  dialogVisible: boolean;
  newInvite: boolean;
  succesMessage: string;
  succesErreurMessage: string;
  errorMessage: string;
  errorMessageStatus: string;
  // selectTtems
 /* titres: SelectItem[];
  statuts: SelectItem[];
  profils: SelectItem[];*/
  // menuItem
/*  items: MenuItem[];
  msgs: Message[];*/

  loading: boolean;

  constructor(private  inviteService: InviteService,
              private msgService: MessageErreurService,
              private dialog: MatDialog) {

  }

  ngOnInit() {
    /*Pour table material*/
    this.matTable();
    /*Fin material*/


    this.loading = true;
    setTimeout(() => {
      this.tousInvites();
      this.loading = false;
    }, 1000);

  /*  this.selectTitres();
    this.selectStatuts();
    this.selectProfils();

    this.items = [
      {label: 'Supprimer', icon: 'fa-close', command: (event) => this.deleteInvite(this.selectedInvite)}
    ];*/
  }

  /*MATERIAL TABLE*/
  matTable() {
    this.inviteService.getAllsInvites().subscribe(res => {
      this.inviteDataSource.data = res.body;

    });
  }

  /*Dialogues*/
  openDiaolDetail(inv: Invite) {
    const dialogRef = this.dialog.open(DialogInviteDetailComponent, {
      width: '400px',
      data: inv
    });
    dialogRef.afterClosed().subscribe(resultat => {
      console.log('Dialogue detail ferme');
      this.invite = resultat;
    });
  }

  openDialogSupprime(invit: Invite) {
    this.invite = invit;
    const dialogRef = this.dialog.open(DialogInviteSupprimeComponent, {
      data: invit
    });
    dialogRef.afterClosed().subscribe(resultat => {
      this.invite = resultat;
      if (resultat) {
        if (this.invite.id !== null) {
          this.delete();
          console.log('Invite supprimer est :', this.invite);
        } else {
          console.log('Suppression impossible.');
        }
        ;
      }
    });
  }


  tousInvites() {
    this.inviteService.getAllsInvites()
      .subscribe(data => {
        this.invites = data.body;
        /*this.statusSucces = data.status;
         this.succesMessage = data.messages.toString();*/
      });
    this.log();
    if (this.errorMessage) {
      console.log('Verifie erreur: ', this.errorMessage);
    }
    // this.msgService.message$.subscribe(m => console.log('Verifie erreur: ', m));
  }

  voir(i: Invite) {
    console.log('Material essai', i);
  }

  voir2(i: Invite) {
    console.log('Material double click', i);
  }

  ngAfterViewInit() {
    this.inviteDataSource.paginator = this.paginator;
    this.inviteDataSource.sort = this.sort;
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.inviteDataSource.filter = filterValue;
  }

  showDialogToAdd() {
    this.newInvite = true;

    const ad = new Adresse('', '', '', '', '', '');
    this.invite = new Invite(null, null, '', ad, null,
      null, null, null, null, false, null,
      null, null, null, null, null);
    // this.displayDialog = true;
    const dialogRef = this.dialog.open(DialogInviteCrudComponent, {
      width: '410px',
      data: this.invite
    });
    dialogRef.afterClosed().subscribe(resultat => {
      // this.invite = resultat;
      console.log('ESSAI DIALOGUE AJOUT', this.invite);
    //  console.log('ESSAI DIALOGUE AJOUT 2', resultat);
      if(resultat){
        this.save();
      }
    });
  }

  cancel() {
    this.displayDialog = false;
    this.dialogVisible = false;
  }

  showInvite(inv: Invite) {
    this.newInvite = false;
    this.invite = inv;
    this.selectedInvite = inv;
    this.invite = this.cloneInvite(inv);
    // this.dialogVisible = true;
    // this.invite = this.selectedInvite;
    const dialogRef = this.dialog.open(DialogInviteCrudComponent, {
      width: '410px',
      data: this.invite
    });
    dialogRef.afterClosed().subscribe(resultat => {
     //  this.invite = resultat;
      if(resultat){
        this.save();
      }
    });
  }

  save() {
    const invites = [...this.invites];
    if (this.newInvite) {
      this.inviteService.ajoutInvite(this.invite)
        .subscribe(res => {
            if (res.status === 0) {
              // invites.push(res.body);
              // this.inviteDataSource.data.push(res.body);
              this.matTable();
              this.inviteDataSource.connect();
              // this.invites = invites;
              this.statusSucces = res.status;
              this.succesMessage = res.messages.toString();
              // this.invites = invites;
              console.log('Ajout datasource', this.inviteDataSource.data);

            } else {
              this.statusSucces = res.status;
              this.succesErreurMessage = res.messages.toString();
              console.log(res);
            }
          },
          error => this.errorMessage = error);

    } else {
      console.log('modif debut');
      console.log(this.invite);
      this.inviteService.editerInvitet(this.invite)
        .subscribe(res => {
            if (res.status === 0) {
              // invites[this.findSelectedInviteIndex()] = res.body;
              // this.invites = invites;
              this.matTable();
              this.inviteDataSource.connect();

              this.statusSucces = res.status;
              this.succesMessage = res.messages.toString();
            } else {
              this.statusSucces = res.status;
              this.succesMessage = res.messages.toString();
            }
          },
          resError => {
            this.errorMessage = resError;
            console.error(resError);
          }
        );
    }

    this.invite = null;
    this.displayDialog = false;
    this.dialogVisible = false;
    this.closeMessage();
    console.log(this.statusSucces);
    console.log(this.succesMessage);

    this.log();
    if (this.errorMessage) {
      console.log('Verifie erreur: ', this.errorMessage);
    }

  }

  delete() {
    const index = this.findSelectedInviteIndex();
    this.inviteService.suppimeInvite(this.invite.id)
      .subscribe(res => {
        if (res.status === 0) {
          this.succesMessage = res.messages.toString();
          this.statusSucces = res.status;
          this.matTable();
          this.inviteDataSource.connect();
          if(this.inviteDataSource.connect().value.length === 1){
            this.paginator.previousPage();
          }
          console.log(res);
          //  console.log('Verifie pagination', this.inviteDataSource.connect());
        } else {
          this.succesMessage = res.messages.toString();
          this.statusSucces = res.status;
          console.log(res);
        }
      }, erreur => this.errorMessage = erreur);
    this.invite = null;
    this.displayDialog = false;
    this.closeMessage();
  }

  onRowSelect(event) {
    this.newInvite = false;
    this.invite = this.cloneInvite(event.data);
    this.displayDialog = true;
    console.log(event.data);
    console.log(this.invite);
  }

  findSelectedInviteIndex(): number {
    return this.invites.indexOf(this.selectedInvite);
  }

  cloneInvite(i: Iinvite): Invite {
    // cloner adresse
    let add: Adresse;
    add = new Adresse(i.adresse.quartier,
      i.adresse.codePostal,
      i.adresse.email,
      i.adresse.mobile,
      i.adresse.bureau,
      i.adresse.tel);
    // cloner invite
    let invite: Invite;
    invite = new Invite();
    for (const prop in i) {
      invite[prop] = i[prop];
    }
    invite.adresse = add;
    return invite;
  }

  private closeMessage() {
    setTimeout(() => {
      this.succesMessage = '';
      this.succesErreurMessage = '';
      this.errorMessage = '';
      this.errorMessageStatus = '';
      this.statusSucces = null;
    }, 5000);
  }

  // pour DropDowns
 /* selectTitres() {
    this.titres = [];
    this.titres.push({label: 'Mme', value: 'Mme'});
    this.titres.push({label: 'Mlle', value: 'Mlle'});
    this.titres.push({label: 'Mr', value: 'Mr'});
    this.titres.push({label: 'Dr', value: 'Dr'});
    this.titres.push({label: 'Me', value: 'Me'});
  }*/

 /* selectStatuts() {
    this.statuts = [];
    this.statuts.push({label: 'Temporaire', value: 'Tempo'});
    this.statuts.push({label: 'Permanant', value: 'Perm'});
  }*/

 /* selectProfils() {
    this.profils = [];
    this.profils.push({label: 'Client', value: 'Client'});
    this.profils.push({label: 'Partenaire', value: 'Partenaire'});
    this.profils.push({label: 'Visiteur', value: 'Visiteur'});
  }*/

  /*deleteInvite(inv: Invite) {
    this.confirmDelete(inv);
  }*/

  /*confirmDelete(inv: Invite) {
    this.confirmationService.confirm({
      message: `Voulez-vous suppimer l'invité de ID:${inv.id} ${inv.numCni} ${inv.nom} ${inv.prenom}`,
      header: 'Confirmation de suppression',
      icon: 'fa-fa-trash',
      accept: () => {
        this.msgs = [];
        this.selectedInvite = inv;
        this.invite = this.selectedInvite;
        console.log('Confirme');
        console.log(this.invite);
        this.delete();
        this.msgs.push({severity: 'info', summary: 'Comfirmation', detail: 'Visiteur supprimé.'});
      }
    });
  }*/

  private log() {
    this.msgService.message$.subscribe(ms => {
      this.errorMessage = ms;
      console.log('Message erreur; ', ms);
    });
  }

}


/*Dialogue pour invites*/

