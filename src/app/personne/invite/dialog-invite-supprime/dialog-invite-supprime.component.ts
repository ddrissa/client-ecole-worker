import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {Invite} from '../../../shared/model/personne/invite';

@Component({
  selector: 'app-dialog-invite-supprime',
  templateUrl: './dialog-invite-supprime.component.html',
  styleUrls: ['./dialog-invite-supprime.component.scss']
})
export class DialogInviteSupprimeComponent {

  constructor(public  dialogRef: MatDialogRef<DialogInviteSupprimeComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Invite) {
  }

  onNoClick() {
    this.dialogRef.close();
  }

}
