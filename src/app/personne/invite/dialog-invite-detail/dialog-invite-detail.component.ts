import {Component, Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Invite} from '../../../shared/model/personne/invite';


@Component({
  selector: 'app-dialog-invite-detail',
  templateUrl: './dialog-invite-detail.component.html',
  styleUrls: ['./dialog-invite-detail.component.scss']
})
export class DialogInviteDetailComponent {

  constructor(public dialogRef: MatDialogRef<DialogInviteDetailComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Invite) {
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}
