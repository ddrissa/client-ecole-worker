import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EmployeComponent} from './employe.component';
import {EmployeManageComponent} from './employe-manage/employe-manage.component';
import {EmployeListComponent} from './employe-list/employe-list.component';
import {EmployeDebutComponent} from './employe-debut/employe-debut.component';
import {EmployeEditComponent} from './employe-edit/employe-edit.component';
import {EmployePhotoComponent} from './employe-photo/employe-photo.component';
import {EmployeDetailComponent} from './employe-detail/employe-detail.component';

const routes: Routes = [
  {
    path: 'employe', component: EmployeComponent,
    children: [
      {path: '', component: EmployeManageComponent},
      {path: 'list', component: EmployeListComponent,
      children:[
        {path: '', component: EmployeDebutComponent},
        {path: 'creer', component: EmployeEditComponent},
        {path: 'newphoto', component: EmployePhotoComponent},
        {path: ':id', component: EmployeDetailComponent},
        {path: ':id/edit', component: EmployeEditComponent},
        {path: ':id/photo', component: EmployePhotoComponent}
      ]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeRoutingModule {
}
