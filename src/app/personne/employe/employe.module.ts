import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EmployeRoutingModule} from './employe-routing.module';
import {EmployeComponent} from './employe.component';
import {EmployeManageComponent} from './employe-manage/employe-manage.component';
import {EmployeDebutComponent} from './employe-debut/employe-debut.component';
import {EmployeDetailComponent} from './employe-detail/employe-detail.component';
import {EmployeEditComponent} from './employe-edit/employe-edit.component';
import {EmployeListComponent} from './employe-list/employe-list.component';
import {EmployePhotoComponent} from './employe-photo/employe-photo.component';
import {MaterialModule} from '../../material.module';
import {EmployeService} from '../../shared/service/personne/employe/employe.service';
import {ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EmployeRoutingModule,
    MaterialModule,
    TextMaskModule
  ],
  declarations: [
    EmployeComponent,
    EmployeManageComponent,
    EmployeDebutComponent,
    EmployeDetailComponent,
    EmployeEditComponent,
    EmployeListComponent,
    EmployePhotoComponent],
  providers:[
    EmployeService
  ]
})
export class EmployeModule {
}
