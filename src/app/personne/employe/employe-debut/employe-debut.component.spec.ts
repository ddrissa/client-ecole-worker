import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeDebutComponent } from './employe-debut.component';

describe('EmployeDebutComponent', () => {
  let component: EmployeDebutComponent;
  let fixture: ComponentFixture<EmployeDebutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeDebutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeDebutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
