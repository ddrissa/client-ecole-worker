import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployePhotoComponent } from './employe-photo.component';

describe('EmployePhotoComponent', () => {
  let component: EmployePhotoComponent;
  let fixture: ComponentFixture<EmployePhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployePhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployePhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
