import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeManageComponent } from './employe-manage.component';

describe('EmployeManageComponent', () => {
  let component: EmployeManageComponent;
  let fixture: ComponentFixture<EmployeManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
