import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantDebutComponent } from './enseignant-debut.component';

describe('EnseignantDebutComponent', () => {
  let component: EnseignantDebutComponent;
  let fixture: ComponentFixture<EnseignantDebutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnseignantDebutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantDebutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
