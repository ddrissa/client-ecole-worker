import {Component, OnInit} from '@angular/core';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Enseignant} from '../../../shared/model/personne/enseignant';
import {Resultat} from '../../../shared/model/resultat';
import {Adresse} from '../../../shared/model/personne/adresse';
import {Observable} from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import {HttpErrorResponse} from '@angular/common/http';
import emailMask from 'text-mask-addons/dist/emailMask';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';

@Component({
  selector: 'app-enseignant-edit',
  templateUrl: './enseignant-edit.component.html',
  styleUrls: ['./enseignant-edit.component.scss']
})
export class EnseignantEditComponent implements OnInit {
  resultat: Resultat<Enseignant>;
  enseignant: Enseignant;
  enseignantForm: FormGroup;
  // enseignantForm2: FormGroup;
  id: number;
  editMode = false;
  msgSucces: string;
  erreurMessage: string;

  // TextMask
  maskPhone = ['(', '+', '2', '2', '5', ')', ' ', /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  /*cni Variable*/
 // maskCni = [/[A-Z]/i, /[A-Z]/i, /[A-Z]/i, '-', /\d/, /\d/, /\d/, /\d/];
  maskCni = ['C', 'N', 'I', '-', /\d/, /\d/, /\d/, /\d/];
  emailMask = emailMask;

  titres = [
    {label: 'Mlle', name: 'Mlle'},
    {label: 'Mme', name: 'Mme'},
    {label: 'Mr', name: 'Mr'}
  ];
  typeTel = [
    {label: 'Bureau', titre: 'Bureau'},
    {label: 'Domicile', titre: 'Domicile'},
    {label: 'Fixe', titre: 'Fixe'},
    {label: 'Mobile', titre: 'Mobile'}
  ];

  constructor(private enseignantService: EnseignantService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar) {

  }

  ngOnInit() {

    this.route.paramMap.pipe(switchMap((params: ParamMap) => {
      this.id = +params.get('id');
      this.editMode = params.get('id') != null;
      if (this.editMode) {
        return this.enseignantService.getEnseignatById(this.id);
      } else {
        this.newInit();
        return of(new Resultat(9, ['Nouveau'], new Enseignant()));
      }

    }))
      .subscribe(res => {
        this.resultat = res;
        this.enseignant = res.body;
        if (res.status === 0) {
          this.initForm();
          console.log('editMode: ', this.editMode);
          console.log('editMode body: ', res);
          console.log('enseignantForm: ', this.enseignantForm.value);
        }


      },
        (err: HttpErrorResponse) => {
        if (err instanceof Error){
          // Erreur cote client  ou de reseau
          this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
          console.log('Une erreur est servenue', err.error.message);
        }else {
          // Erreur cote serveur ou backend
          this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
          console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
        }
        });

    console.log('Rsultat affecte: ', this.resultat);
  }


  /* openSnackBar(message: string) {
     this.snackBar.open(message, ' ', {
       duration: 5000,
     });
   }*/

  onSubmit() {
    if (!this.editMode) {
      const ens = this.converteur(this.enseignantForm);
      this.enseignantService.ajoutEnseignant(ens)
        .subscribe(res => {
          this.msgSucces = res.messages.toString();
          // this.openSnackBar(this.msgSucces);
          console.log('Lenseignant ajouter', res);
          console.log('Lenseignant message', res.messages);
        },
          (err: HttpErrorResponse) => {
            if (err instanceof Error){
              // Erreur cote client  ou de reseau
              this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
              console.log('Une erreur est servenue', err.error.message);
            }else {
              // Erreur cote serveur ou backend
              this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
              console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
            }
          });

    } else {
      const ensModif = this.converteur(this.enseignantForm);
      this.enseignantService.modifierEnseignant(ensModif)
        .subscribe(res => {
          this.msgSucces = '';
          this.msgSucces = res.messages.toString();
          //  this.openSnackBar(this.msgSucces);
          console.log('Retour modifier:', res);
        },
          (err: HttpErrorResponse) => {
            if (err instanceof Error){
              // Erreur cote client  ou de reseau
              this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
              console.log('Une erreur est servenue', err.error.message);
            }else {
              // Erreur cote serveur ou backend
              this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
              console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
            }
          });
      console.log('Modification', this.enseignantForm.value);

    }
    ;

    this.annuler();
    console.log('Enseignant submit ', this.converteur(this.enseignantForm));

  }


  addTelephone() {
    (<FormArray>this.enseignantForm.get('telephones')).push(
      this.fb.group({
        titre: [''],
        numero: [''],
        version: [0],
        id: [null]
      })
    );
  }


  removeTelephone(i: number) {
    (<FormArray>this.enseignantForm.get('telephones')).removeAt(i);
  }

  annuler() {
    this.router.navigate(['enseignant/list']);
  }

  /*INITIALISATION*/
  private initForm() {
    const telephoneArray = new FormArray([]);
    let ens: Enseignant;
    if (this.editMode) {
      if (this.resultat.status === 0) {
        ens = this.enseignant;
        console.log('ens init', ens);
        if (ens.telephones.length !== 0) {
          for (const tel of ens.telephones) {

            telephoneArray.push(
              this.fb.group({
                titre: tel.titre,
                numero: tel.numero,
                version: tel.version,
                id: tel.id
              })
            );
          }
        }
      }
    }

    this.enseignantForm = this.fb.group({
      id: [ens.id],
      version: [ens.version],
      cni: [ens.numCni, Validators.required],
      titre: [ens.titre],
      nom: [ens.nom, Validators.required],
      prenom: [ens.prenom, Validators.required],
      statut: [ens.status],
      pathphoto: [ens.pathphoto],
      specialite: [ens.specialite],
      telephones: telephoneArray,
      adresse: this.fb.group({
        quartier: [ens.adresse.quartier],
        codePostal: [ens.adresse.codePostal],
        email: [ens.adresse.email],
        mobile: [ens.adresse.mobile],
        bureau: [ens.adresse.bureau],
        tel: [ens.adresse.tel]
      })

    });

    console.log('Dans initforms on :', this.enseignantForm.value);
  }


  // AJOUTER
  private newInit() {
    const ens = new Enseignant(null, 0, '',
      new Adresse('', '', '', '', '', ''),
      '', '', '', null, null, false, null, null, [], '', null, null);


    this.enseignantForm = this.fb.group({
      id: [ens.id],
      version: [ens.version],
      cni: [ens.numCni, Validators.required],
      titre: [ens.titre],
      nom: [ens.nom, Validators.required],
      prenom: [ens.prenom, Validators.required],
      statut: [ens.status],
      pathphoto: [ens.pathphoto],
      specialite: [ens.specialite],
      adresse: this.fb.group({
        quartier: [ens.adresse.quartier],
        codePostal: [ens.adresse.codePostal],
        email: [ens.adresse.email],
        mobile: [ens.adresse.mobile],
        bureau: [ens.adresse.bureau],
        tel: [ens.adresse.tel]
      }),
      telephones: this.fb.array([
        this.fb.group({
          titre: [''],
          numero: [''],
          version: [0],
          id: [null]
        })
      ])
    });
  }

  private converteur(fm: FormGroup): Enseignant {
    const newEnseig = new Enseignant(
      fm.value['id'],
      fm.value['version'],
      fm.value['titre'],
      fm.value['adresse'],
      fm.value['nom'],
      fm.value['prenom'],
      fm.value['cni'],
      null,
      null,
      true,
      fm.value['EN'],
      fm.value['pathphoto'],
      fm.value['telephones'],
      null,
      fm.value['statut'],
      fm.value['specialite']);

    return newEnseig;
  }

}
