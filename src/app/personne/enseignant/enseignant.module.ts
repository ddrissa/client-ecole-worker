import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EnseignantRoutingModule} from './enseignant-routing.module';
import {EnseignantListComponent} from './enseignant-list/enseignant-list.component';
import {EnseignantEditComponent} from './enseignant-edit/enseignant-edit.component';
import {EnseignantDetailComponent} from './enseignant-detail/enseignant-detail.component';
import {EnseignantComponent} from './enseignant.component';
import {EnseignantDebutComponent} from './enseignant-debut/enseignant-debut.component';
import {EnseignantManageComponent} from './enseignant-manage/enseignant-manage.component';
import {EnseignantService} from '../../shared/service/personne/enseignant/enseignant.service';
import {EnseignantPhotoComponent} from './enseignant-photo/enseignant-photo.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../material.module';
import {TextMaskModule} from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EnseignantRoutingModule,
    MaterialModule,
    TextMaskModule
  ],
  declarations: [
    EnseignantListComponent,
    EnseignantEditComponent,
    EnseignantDetailComponent,
    EnseignantComponent,
    EnseignantDebutComponent,
    EnseignantManageComponent,
    EnseignantPhotoComponent],
  providers: [
    EnseignantService
  ]
})
export class EnseignantModule {
}
