import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantPhotoComponent } from './enseignant-photo.component';

describe('EnseignantPhotoComponent', () => {
  let component: EnseignantPhotoComponent;
  let fixture: ComponentFixture<EnseignantPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnseignantPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
