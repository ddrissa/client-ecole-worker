import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EnseignantComponent} from './enseignant.component';
import {EnseignantListComponent} from './enseignant-list/enseignant-list.component';
import {EnseignantDetailComponent} from './enseignant-detail/enseignant-detail.component';
import {EnseignantManageComponent} from './enseignant-manage/enseignant-manage.component';
import {EnseignantDebutComponent} from './enseignant-debut/enseignant-debut.component';
import {EnseignantEditComponent} from './enseignant-edit/enseignant-edit.component';
import {EnseignantPhotoComponent} from './enseignant-photo/enseignant-photo.component';

const routes: Routes = [
  {
    path: 'enseignant', component: EnseignantComponent,
    children: [
      {
        path: '', component: EnseignantManageComponent,
      },
      {
        path: 'list', component: EnseignantListComponent,
        children: [
          {path: '', component: EnseignantDebutComponent},
          {path: 'creer', component: EnseignantEditComponent},
          {path: 'newphoto', component: EnseignantPhotoComponent},
          {path: ':id', component: EnseignantDetailComponent},
          {path: ':id/edit', component: EnseignantEditComponent},
          {path: ':id/photo', component: EnseignantPhotoComponent}
        ],
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnseignantRoutingModule {
}
