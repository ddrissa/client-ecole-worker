import {Component, OnInit} from '@angular/core';

import {ActivatedRoute, Router} from '@angular/router';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Enseignant} from '../../../shared/model/personne/enseignant';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Observable, Subject, BehaviorSubject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, retry, switchMap, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';


import {MessageErreurService} from '../../../shared/service/messageErreur.service';
import {pipe} from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-enseignant-list',
  templateUrl: './enseignant-list.component.html',
  styleUrls: ['./enseignant-list.component.scss']
})
export class EnseignantListComponent implements OnInit {
  ensignants: Enseignant[];
  oEnseignants: Observable<Enseignant[]>;
  private valeurChercher = new BehaviorSubject<string>(' ');
  selectedEnseignant: Enseignant;
  succesErreur: string;
  erreurMessage: string;
  erreurMotCleMessage: string;
  succesMessage: string;
  tailleEsn: number;
  statut = 0;
  nonvide = false;
  trouve = false;
  pathNullImage = './assets/image/ensNull.png';

  constructor(private enseignantService: EnseignantService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private msgErrService: MessageErreurService) {
  }

  ngOnInit() {
    this.enseignantAll();
    // Ajouter enseignant creer
    this.enseignantService.reponseStatut$
      .subscribe(n => {
        if (this.ensignants.length === 0) {
          this.statut = n;
          console.log('nonvide apres ajouter: ', this.nonvide);
        }

      });

    // Filtrer par communication
    this.enseignantService.enseignantFilter$
      .subscribe(t => {
        this.rechercher(t);
        this.initialiseList();
      });

    // enseignants chercher ////////////////
    this.oEnseignants = this.valeurChercher.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(v => v ? this.enseignantService.searchEnsMotcle(v) : this.enseignantService.searchEnsMotcle(' ')),
        pipe(
        map(res => res.body),
        tap(d => {
          console.log('Erreur motcle', this.log())
        }),
      )

    )





    // CODE SANS RCHERCHE
    // Ajouter enseignant creer
    this.enseignantService.enseignantCreer$
       .subscribe(res => {
         this.ensignants.push(res.body);
         this.succesMessage = res.messages.toString();
         this.openSnackBar(this.succesMessage);
         console.log('Enseignant ajouter: ', res);
       });
     // Enseignant modifier
     this.enseignantService.enseignantModifier$
       .subscribe(res => {
         const index = this.findSelectedEnseignantIndex();
         this.ensignants[index] = res.body;
         this.succesMessage = res.messages.toString();
         this.openSnackBar(this.succesMessage);
         console.log('index', index);
       });

     // enseignant supprimer
     this.enseignantService.enseignantSupprime$
       .subscribe(res => {
         const index = this.findSelectedEnseignantIndex();
         this.ensignants = this.ensignants.filter((val, i) => i !== index);
         this.succesMessage = res.messages.toString();
         this.openSnackBar(this.succesMessage);
         console.log('Enseignat supprimer: ', res);
       })


  }


  enseignantAll() {
    this.enseignantService.getAllEnseignant()
      .pipe(retry(3))
      .subscribe(data => {
          this.statut = data.status;
          this.nonvide = data.status === 0;
          this.ensignants = data.body;
          this.succesErreur = data.messages.toString();
          console.log('Verifie non vide: ', this.nonvide);
          // console.log('Tous enseignants: ', data);
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            // Erreur cote client  ou de reseau
            this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
            console.log('Une erreur est servenue', err.error.message);
          } else {
            // Erreur cote serveur ou backend
            this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
            console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
          }
          this.closeMessage();
        });
  }

  addProf() {
    this.router.navigate(['creer'], {relativeTo: this.route});
  }

  onSelect(e: Enseignant) {
    this.selectedEnseignant = e;
    console.log('Selectionner:', this.selectedEnseignant);
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, ' ', {
      duration: 5000,
    });
  }

////// index
  findSelectedEnseignantIndex(): number {
    return this.ensignants.indexOf(this.selectedEnseignant);
  }

  // Recherche
  rechercher(v: string) {
    this.valeurChercher.next(v);
    console.log('Tape', v);
  };

  private initialiseList() {
    setTimeout(() => {

      this.rechercher(' ');
    }, 15000);
  };

  /// todo : recuperer les messages d'erreur
  private log() {
    this.msgErrService.message$.subscribe(ms => {
      this.erreurMotCleMessage = ms;
      console.log('Message erreur dans service; ', ms);
      this.closeMessage();
    });


  }

  private closeMessage() {
    setTimeout(() => {
      this.succesMessage = '';
      this.erreurMotCleMessage = '';
      this.erreurMessage = '';
    }, 5000);
  }
}
