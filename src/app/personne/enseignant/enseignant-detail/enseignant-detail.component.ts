import {Component, OnInit} from '@angular/core';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Enseignant} from '../../../shared/model/personne/enseignant';
import {HttpErrorResponse} from '@angular/common/http';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-enseignant-detail',
  templateUrl: './enseignant-detail.component.html',
  styleUrls: ['./enseignant-detail.component.scss']
})
export class EnseignantDetailComponent implements OnInit {
  enseignant: Enseignant;
  pathNullImage = './assets/image/ensNull.png';
  erreurMessage: string;

  constructor(private enseignantService: EnseignantService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.pipe(switchMap((params: Params) => this.enseignantService.getEnseignatById(+params['id'])))
      .subscribe(res => {
        this.enseignant = res.body;
        console.log('Enseignant trouer ', res.body);
      },
        (err: HttpErrorResponse) => {
          if (err instanceof Error){
            // Erreur cote client  ou de reseau
            this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
            console.log('Une erreur est servenue', err.error.message);
          }else {
            // Erreur cote serveur ou backend
            this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
            console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
          }
        });
  }

  editer() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  supprimer(e: Enseignant) {
    this.enseignantService.supprimerEnseignat(e.id)
      .subscribe(res => {
        console.log('Suppression :', res);
      },
        (err: HttpErrorResponse) => {
          if (err instanceof Error){
            // Erreur cote client  ou de reseau
            this.erreurMessage = `Une erreur est servenue ${err.error.message}`;
            console.log('Une erreur est servenue', err.error.message);
          }else {
            // Erreur cote serveur ou backend
            this.erreurMessage = `Backend code retourner: ${err.status}, le contenu; ${err.error}`;
            console.log(`Backend code retourner: ${err.status}, le contenu: ${err.error}`);
          }
        });
    this.router.navigate(['enseignant/list']);
  }

  photo() {
    this.router.navigate(['photo'], {relativeTo: this.route});
  }
}
